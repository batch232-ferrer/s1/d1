public class Main {
    //  Main Class
        // Entry point for our Java program.
        // Main class has 1 method inside which is the 'main' method => to run our code
    public static void main(String[] args) {
        /*
            "public" - access modifier which simply tells the app which classes have access to our method/attributes.
            "static" - method/property that belongs to the class. It is accessible without having to create an instance of an object.
            "void" - method will not return any data. Because in Java we have to declare the data type of the method's return.
        */
        // System.out.printIn is a statement that allows us to print the value of the arguments passed into its terminal.
        System.out.println("Hello world!");
        System.out.println("Anthony Ferrer");
    }
}
