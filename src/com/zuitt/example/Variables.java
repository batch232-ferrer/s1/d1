package com.zuitt.example;

public class Variables {
    public static void main(String[] args){
        // Variable
        int age;
        char middle_name;

        // Variable initialization
        int x = 0;
        age = 18;

        // int myNum = "sampleString" -> will result in an error
        // L "i" added at the end of the Long number to be recognized as long. Otherwise, it recognized as int.
        long worldPopulation = 782688145L;
        System.out.println(worldPopulation);

        // float - we have to add "f" at the end of a float to be recognized as float
        float piFloat = 3.14159f;
        System.out.println(piFloat);

        // double
        double piDouble = 3.1415926;
        System.out.println(piDouble);

        // char - can only hold 1 character
        // uses single quotes
        char letter = 'a';
        System.out.println(letter);

        // boolean
        boolean isMVP = true;
        boolean isChampion = false;
        System.out.println(isMVP);
        System.out.println(isChampion);

        // Constants in Java
        // final dataType <VARIABLE NAME>
        final int PRINCIPAL = 3000;
        System.out.println((PRINCIPAL));
        // cannot be reassigned
        // PRINCIPAL - 4000;

        // String - non-primitive data type. Strings are objects that can use methods.

        String username = "theTinker23";
        System.out.println(username);
        // isEmpty - string method which returns a boolean
        // checks the length of the string, returns true if > 0, otherwise, false.
        System.out.println((username.isEmpty())); // result: false
    }
}
